data "ignition_user" "user" {
  name = "${var.serviceuser.username}"
  uid = var.serviceuser.uid
}

data "ignition_directory" "usr_local" {
  path = "${var.serviceuser.home}/.local"
  mode = 488
  uid = var.serviceuser.uid
  gid = var.serviceuser.gid
}

data "ignition_directory" "usr_bin" {
  path = "${var.serviceuser.home}/.local/bin"
  mode = 488
  uid = var.serviceuser.uid
  gid = var.serviceuser.gid
}

data "ignition_directory" "bashrc" {
  path = "${var.serviceuser.home}/.bashrc.d"
  mode = 493
  uid = var.serviceuser.uid
  gid = var.serviceuser.gid
}

data "ignition_file" "bash_exports" {
  path = "${var.serviceuser.home}/.bashrc.d/exports.bash"
  content {
    content = "${file("${path.module}/templates/exports.bash")}"
  }
  mode = 384
  uid = var.serviceuser.uid
  gid = var.serviceuser.gid
}

data "ignition_directory" "user_config" {
  path = "${var.serviceuser.home}/.config"
  mode = 493
  uid = var.serviceuser.uid
  gid = var.serviceuser.gid
}

data "ignition_directory" "user_containers" {
  path = "${var.serviceuser.home}/.config/containers"
  mode = 493
  uid = var.serviceuser.uid
  gid = var.serviceuser.gid
}

data "ignition_directory" "systemd_containers" {
  path = "${var.serviceuser.home}/.config/containers/systemd"
  mode = 493
  uid = var.serviceuser.uid
  gid = var.serviceuser.gid
}

data "ignition_directory" "systemd_config" {
  path = "${var.serviceuser.home}/.config/systemd"
  mode = 493
  uid = var.serviceuser.uid
  gid = var.serviceuser.gid
}

data "ignition_directory" "systemd_user" {
  path = "${var.serviceuser.home}/.config/systemd/user"
  mode = 493
  uid = var.serviceuser.uid
  gid = var.serviceuser.gid
}

data "ignition_directory" "systemd_targets" {
  path = "${var.serviceuser.home}/.config/systemd/user/default.target.wants"
  mode = 493
  uid = var.serviceuser.uid
  gid = var.serviceuser.gid
}

data "ignition_directory" "systemd_sockets" {
  path = "${var.serviceuser.home}/.config/systemd/user/sockets.target.wants"
  mode = 493
  uid = var.serviceuser.uid
  gid = var.serviceuser.gid
}

data "ignition_directory" "systemd_timers" {
  path = "${var.serviceuser.home}/.config/systemd/user/timers.target.wants"
  mode = 493
  uid = var.serviceuser.uid
  gid = var.serviceuser.gid
}

data "ignition_file" "linger" {
  path = "/var/lib/systemd/linger/${var.serviceuser.username}"
  mode = 420
  content {
    content = ""
  }
}

data "ignition_config" "config" {
  users = [data.ignition_user.user.rendered]

  directories = [
    data.ignition_directory.usr_local.rendered,
    data.ignition_directory.usr_bin.rendered,
    data.ignition_directory.user_config.rendered,
    data.ignition_directory.user_containers.rendered,
    data.ignition_directory.systemd_config.rendered,
    data.ignition_directory.systemd_user.rendered,
    data.ignition_directory.systemd_containers.rendered,
    data.ignition_directory.systemd_targets.rendered,
    data.ignition_directory.systemd_sockets.rendered,
    data.ignition_directory.systemd_timers.rendered,
    data.ignition_directory.bashrc.rendered,
  ]

  files = [
    data.ignition_file.linger.rendered,
    data.ignition_file.bash_exports.rendered,
  ]
}

variable "serviceuser" {
  type = object({
    username = string
    home = string
    uid = number
    gid = number
  })
}

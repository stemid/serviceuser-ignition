# Service user base

All my services deployed using Terraform ignition modules depend on this module.

This creates a service user on CoreOS and prepares it to run rootless podman containers, along with some other convenient things like bashrc config and paths necessary to run systemd timers.

The home, uid and gid must be set statically because ignition needs them in other modules to set ownership of files.

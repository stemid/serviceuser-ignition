output "config" {
  value = data.ignition_config.config
}

# Provide all ignition objects as output so a calling module can put together
# its own ignition_config that results in smaller total size.
output "ignition_user" {
  value = [data.ignition_user.user]
}

output "ignition_file" {
  value = [
    data.ignition_file.bash_exports,
    data.ignition_file.linger,
  ]
}

output "ignition_link" {
  value = []
}

output "ignition_systemd_unit" {
  value = []
}

output "ignition_directory" {
  value = [
    data.ignition_directory.usr_local,
    data.ignition_directory.usr_bin,
    data.ignition_directory.bashrc,
    data.ignition_directory.user_config,
    data.ignition_directory.user_containers,
    data.ignition_directory.systemd_containers,
    data.ignition_directory.systemd_config,
    data.ignition_directory.systemd_user,
    data.ignition_directory.systemd_targets,
    data.ignition_directory.systemd_sockets,
    data.ignition_directory.systemd_timers,
  ]
}
